import pygame
pygame.init()

pywindow = pygame.display.set_mode((500, 500))
pygame.display.set_caption('Draw Circle at Cursor!')
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
pywindow.fill(WHITE)
LEFT = 1
RIGHT = 3
rectangle = pygame.rect.Rect(0, 0, 0, 0)
rectangle_draging=False
rect=[]
while True: #main game loop
    
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == LEFT:
                rectangle_draging=True
                x,y=pygame.mouse.get_pos()
                rectangle.x=x
                rectangle.y=y
                
        elif event.type==pygame.MOUSEMOTION:
            if rectangle_draging:
                x2,y2=pygame.mouse.get_pos()
                rectangle.w=x2-rectangle.x
                rectangle.h=y2-rectangle.y
                draw=True
                
        elif event.type==pygame.MOUSEBUTTONUP:
            rect.append(rectangle)
            rectangle=pygame.rect.Rect(0,0,0,0)
            rectangle_draging=False
            draw=False
        if event.type == pygame.QUIT:
            pygame.quit()
            
    pywindow.fill(WHITE)
    if draw:
        pygame.draw.rect(pywindow, BLUE, rectangle, 3)  
    for i in rect:
        pygame.draw.rect(pywindow, BLUE, i, 0)    
    pygame.display.update()