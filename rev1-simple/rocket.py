import numpy as np
import copy

class Rocket:
    def __init__(self,location,target,dna):
        self.location = copy.copy(location)
        self.target=target
        self.velocity = np.zeros([1,2])        
        self.acceleration = np.zeros([1,2])
        self.my_fitness =0
        self.dna = dna
        self.geneCounter=0
        self.hitTarget=False
        
    def fitness(self):
        d=np.linalg.norm(self.location-self.target)
        self.my_fitness = pow(1/d,10)
        
    def run(self):
        self.checkTarget()
        if not self.hitTarget:
            self.applyForce(self.dna.genes[self.geneCounter])
            self.geneCounter = (self.geneCounter+1) % len(self.dna.genes)            
            self.velocity+=self.acceleration
            self.location+=self.velocity
            self.acceleration = np.zeros((1,2))
            
        
    def checkTarget(self):
        d = np.linalg.norm(self.location-self.target)
        if d<12:
            self.hitTarget = True
            
    def applyForce(self,f):
        self.acceleration=f
        
        
    
    def getFitness(self):
        return self.my_fitness
    
    def getDNA(self):
        return self.dna
    def getLocation(self):
        return self.location