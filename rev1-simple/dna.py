import numpy as np


class DNA:
    def __init__(self,lifetime):
        self.maxforce=0.5
        self.genes=[]
        self.lifetime=lifetime
        for i in range(self.lifetime):
            angle= np.random.uniform(0,2*np.pi)
            gene=np.array([np.cos(angle),np.sin(angle)])
            
            gene*=np.random.uniform(0,self.maxforce)
            self.genes.append(gene)
            
    def crossover(self,partner):
        child = DNA(self.lifetime)
        midpoint=np.random.randint(0,self.lifetime)
        child.genes[0:midpoint]=self.genes[0:midpoint]
        if not midpoint==self.lifetime-1:
            child.genes[midpoint:self.lifetime]=partner.genes[midpoint:]
        return child
    
    def mutate(self,mutationRate):
        for i in range(self.lifetime):
            if np.random.random() < mutationRate:
                angle=np.random.uniform(0,2*np.pi)
                gene=np.array([np.cos(angle),np.sin(angle)])     
                gene*=np.random.uniform(0,self.maxforce)
                self.genes[i]=gene
                