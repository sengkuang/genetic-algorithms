import numpy as np


class Rocket:
    def __init__(self,location,target,dna):
        self.location = location
        self.target=target
        self.velocity = np.zeros((1,2))        
        self.acceleration = np.zeros((1,2))
        self.r = 4
        self.fitness =0
        self.dna = dna
        self.geneCounter=0
        self.hitTarget=False
    def fitness(self):
        d=np.linalg.norm(self.location-self.target)
        self.fitness = pow(1/d,2)
    def run(self):
        self.checkTarget()
        if not self.hitTarget:
            self.applyForce(self.dna.genes[self.geneCounter])
            self.geneCounter = (self.geneCounter+1) % self.dna.genes.length
            self.update()
            
        
    def checkTarget(self):
        d = np.linalg.norm(self.location-self.target)
        if d<12:
            self.hitTarget = True
    def applyForce(self,f):
        self.acceleration+=f
        
    def update(self):
        self.velocity+=self.acceleration
        self.location+=self.velocity
        self.acceleration = np.zeros((1,2))
    
    def getFitness(self):
        return self.fitness
    def getDNA(self):
        return self.dna
