import math
import sys

import os
import pygame
import numpy as np

import traceback
import logging
import time

from population import Population

def load_image_convert_alpha(filename):
    """Load an image with the given filename from the images directory"""
    
    image= pygame.image.load(os.path.join('images', filename)).convert_alpha()
    return pygame.transform.scale(image,(int(image.get_width()/2),int(image.get_height()/2)))

def draw_centered(surface1, surface2, position):
        """Draw surface1 onto surface2 with center at position"""
        rect = surface1.get_rect()
        rect = rect.move(position[0]-rect.width/2, position[1]-rect.height/2)
        surface2.blit(surface1, rect)
        
def rotate_center(image, rect, angle):
        """rotate the given image around its center & return an image & rect"""
        rotate_image = pygame.transform.rotate(image, angle)
        rotate_rect = rotate_image.get_rect(center=rect.center)
        return rotate_image,rotate_rect
    
    

    
#class Rocket_Object(GameObject):
#    def __init__(self, position):
#        """initializing an Spaceship object given it's position"""
#        super(Rocket_Object, self).__init__(position,\
#            load_image_convert_alpha('rocket.png'))
#        
#        self.image_on = load_image_convert_alpha('rocket.png')
#        self.direction = [0, -1]
#        self.angle = 0
#
#        # a list to hold the missiles fired by the spaceship
#        # (that are active (on the screen))
#        self.active_missiles = []
#
#    def draw_on(self, screen):
#        """Draw the spaceship on the screen"""
#        new_image, rect = rotate_center(self.image, \
#                self.image.get_rect(), self.angle)
#        
#        draw_centered(new_image, screen, self.position)
#
#
#    def move(self):
#        """Do one frame's worth of updating for the object"""
#        
#        # calculate the direction from the angle variable
#        self.direction[0] = math.sin(-math.radians(self.angle))
#        self.direction[1] = -math.cos(math.radians(self.angle))
#
#        # calculate the position from the direction and speed
#        self.position[0] += self.direction[0]*self.speed
#        self.position[1] += self.direction[1]*self.speed


class Rocket_Object():
    def __init__(self, location):
        """initializing an Spaceship object given it's position"""
        
        self.image_on = load_image_convert_alpha('black_dot.png')
        self.velocity=np.zeros([1,2])
        self.acceleration=np.zeros([1,2])
        self.location=np.array([location])



    def draw_on(self, screen):
        """Draw the spaceship on the screen"""
        new_image, rect = rotate_center(self.image_on, \
                self.image_on.get_rect(), self.angle)
        
        draw_centered(new_image, screen, self.location[0])


    def move(self):
        """Do one frame's worth of updating for the object"""
        angle=np.random.uniform(0,2*np.pi)
        # x,y, horizontal, vertical
        direction=np.array([np.sin(angle),np.cos(angle)])
        magnitude=np.random.uniform(0,1)
        force=magnitude*direction

        self.applyForce(force)
        self.update()
    
    def applyForce(self,f):
        self.acceleration+=f
        
    def update(self):
        self.velocity+=self.acceleration
        self.location+=self.velocity
        self.acceleration=np.zeros([1,2])
        
class Game:
    PLAYING,WELCOME = range(2)
    REFRESH=pygame.USEREVENT
    def __init__(self):
        pygame.init()
        
        self.width = 800
        self.height = 600
        self.screen = pygame.display.set_mode((self.width,self.height))
        
        self.bg_color = 255,255,255
        self.big_font = pygame.font.SysFont(None,100)
        self.medium_font = pygame.font.SysFont(None,50)
        self.small_font = pygame.font.SysFont(None,25)
        self.FPS =30
        pygame.time.set_timer(Game.REFRESH,int(1000/self.FPS))
        self.do_welcome()
        
    def do_welcome(self):
        self.state=Game.WELCOME
        self.welcome_smart_rockets = self.big_font.render("Smart Rockets",
                                                       True,(255,215,0))
        self.welcome_desc = self.medium_font.render(
        "[Click anywhere/press Enter] to begin!",True,(35,207,142))
        
    def do_init(self):
        self.lifeCounter = 0
        self.mutationRate=0.01
        self.population_size=50
        self.start_location = np.array([self.width/2,self.height-24])
        self.target = np.array([self.width/2,24])
        self.lifetime = 200
        self.population = Population(self.mutationRate,self.population_size,
                                     self.start_location,self.target,self.lifetime) 
        
        self.start()     
        
    def start(self):
        self.rocket_object=[]
        for i in range(self.population_size):
            self.rocket_object.append(Rocket_Object((self.width/2,self.height-24)))
            self.rocket_object[i].speed=np.random.randint(1,20)
            self.rocket_object[i].angle=np.random.randint(0,360)
        self.state=Game.PLAYING

        
    def run(self):
        running=True
        self.life_counter=0
        
        while running:
            event=pygame.event.wait()
            self.draw()
            if (event.type==pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or \
                event.type==pygame.QUIT:
                    running=False
            elif event.type==Game.REFRESH:
                if self.state != Game.WELCOME:
                    if self.state == Game.PLAYING:
                        if self.life_counter<self.lifetime:
                            self.physics()
                            self.draw()
                            self.life_counter+=1
                        else:
                            self.life_counter=0
                            self.do_init()
                         
            elif event.type==pygame.MOUSEBUTTONDOWN and\
                (self.state==Game.WELCOME):
                self.do_init()
            elif event.type == pygame.KEYDOWN and \
                event.key == pygame.K_RETURN and\
                (self.state == Game.WELCOME):
                self.do_init()

            else:
                pass
            
    def game_over(self):
        self.state=Game.GAME_OVER

    def physics(self):
        if self.state == Game.PLAYING:
            for i in range(self.population_size):
                self.rocket_object[i].move()
            
    def draw(self):
        self.screen.fill(self.bg_color)
        if self.state!=Game.WELCOME:
            if self.state == Game.PLAYING:
                pygame.draw.rect(self.screen, (0,255,0),(self.width/2-20/2,24-20/2,20,20),0)
                for i in range(self.population_size):
                    self.rocket_object[i].draw_on(self.screen)
                self.info = self.small_font.render("life counter: "+str(self.life_counter),
                                               True,(255,215,0))
                draw_centered(self.info,self.screen,
                     (80,self.info.get_height()+10))
        else:
            draw_centered(self.welcome_smart_rockets,self.screen,
                         (self.width/2,self.height/2-self.welcome_smart_rockets.get_height()))
            draw_centered(self.welcome_desc,self.screen,
                         (self.width/2,self.height/2+self.welcome_desc.get_height()))
        pygame.display.flip()
try:
    Game().run()
except Exception as e:
    logging.error(traceback.format_exc())
finally:
    pygame.quit()
