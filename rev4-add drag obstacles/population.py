import numpy as np
import math
from rocket import Rocket
from dna import DNA


class Population:
    def __init__(self,m,population_size,start_location,target,obs,lifetime):
        self.mutationRate = m
        self.population_size=population_size
        self.start_location=start_location
        self.target=target
        self.obs=obs
        self.population = []
        self.generations = 0
        self.probabilities=[]
        self.hitTarget=0
        for i in range(self.population_size):
            self.population.append(Rocket(self.start_location,self.target,self.obs,DNA(lifetime)))
        self.matingPool = []   
    def live(self):
        self.hitTarget=0
        for i in range(self.population_size):
            self.population[i].run()
            if self.population[i].hitTarget:
                self.hitTarget+=1
                
    def fitness(self):
        for i in self.population:
            i.fitness()


    def selection(self):
        pass
    
    def accept_reject(self,maxFitness):
        while True:
            index=np.random.randint(0,self.population_size)
            partner=self.population[index]
            r=np.random.uniform(0,maxFitness)
            if r<partner.getFitness():
                return partner
     
            
            
            

        
    def reproduction(self):
        new_population=[]
        maxFitness=self.getMaxFitness()
        for i in range(self.population_size):
            mom=self.accept_reject(maxFitness)
            dad=self.accept_reject(maxFitness)
            momgenes=mom.getDNA()
            dadgenes=dad.getDNA()
            child = momgenes.crossover(dadgenes)
            child.mutate(self.mutationRate)
            new_population.append(Rocket(self.start_location,self.target,self.obs,child))
        self.population=new_population
        self.generations+=1
        
    def getGenerations(self):
        return self.generations
    def getMaxFitness(self):
        record = 0
        for i in self.population:
            if i.getFitness()>record:
                record=i.getFitness()
        return record