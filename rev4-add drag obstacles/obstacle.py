
class Obstacle:
    def __init__(self,x,y,w,h):
        self.location=[x,y]
        self.w=w
        self.h=h
    def contains(self,spot):
        if spot[0] > self.location[0] and spot[0]<self.location[0]+self.w and \
            spot[1]>self.location[1] and spot[1]<self.location[1]+self.h:
            return True
        else:
            return False
    def points(self):
        return (self.location[0],self.location[1],self.w,self.h)