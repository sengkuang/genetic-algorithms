import os
import pygame
import numpy as np

import traceback
import logging

from population import Population
from obstacle import Obstacle
from plot import DynamicUpdate

def load_image_convert_alpha(filename):
    """Load an image with the given filename from the images directory"""    
    image= pygame.image.load(os.path.join('images', filename)).convert_alpha()
    return pygame.transform.scale(image,(int(image.get_width()/2),int(image.get_height()/2)))

def draw_centered(surface1, surface2, position):
        """Draw surface1 onto surface2 with center at position"""
        rect = surface1.get_rect()
        rect = rect.move(position[0]-rect.width/2, position[1]-rect.height/2)
        surface2.blit(surface1, rect)
        
def rotate_center(image, rect, angle):
        """rotate the given image around its center & return an image & rect"""
        rotate_image = pygame.transform.rotate(image, angle)
        rotate_rect = rotate_image.get_rect(center=rect.center)
        return rotate_image,rotate_rect
    
        
class Game:
    REFRESH=pygame.USEREVENT
    def __init__(self):
        pygame.init()
        
        self.width = 800
        self.height = 600
        self.screen = pygame.display.set_mode((self.width,self.height))
        
        self.bg_color = 255,255,255
        self.big_font = pygame.font.SysFont(None,100)
        self.medium_font = pygame.font.SysFont(None,50)
        self.small_font = pygame.font.SysFont(None,25)
        self.FPS =30
        pygame.time.set_timer(Game.REFRESH,int(1000/self.FPS))
        self.obs=[]
        self.temp_obs=[]
        self.d=DynamicUpdate()
        self.temp_x=[]
        self.temp_y=[]
        self.d.on_launch("Generation","No. of Points Hit on Target")
        
        
        
    def start(self):
        self.image = load_image_convert_alpha('black_dot.png')  
        self.lifeCounter = 0
        self.mutationRate=0.01
        self.population_size=100
        self.start_location = np.array([self.width/2,self.height-24]).reshape((-1,2))
        self.target = np.array([self.width/2,24])
#        self.obs.append(Obstacle(300,self.height/2,self.width-600,10))
        self.lifetime = 200
        self.population = Population(self.mutationRate,self.population_size,
                                     self.start_location,self.target,self.obs,self.lifetime)       

        for i in range(self.population_size):
            draw_centered(self.image,self.screen,self.population.population[i].location[0])


        
    def run(self):
        self.start()
        running=True
        self.life_counter=0
        self.draw()
        obs=pygame.rect.Rect(0,0,0,0)
        mouse_draging=False
        
        while running:
            event=pygame.event.wait()
            if (event.type==pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or \
                event.type==pygame.QUIT:
                    running=False
            elif event.type==Game.REFRESH:
                if self.life_counter<self.lifetime:
                    self.population.live()
                    self.draw()
                    self.life_counter+=1
                else:
                    self.life_counter=0
                    self.temp_x.append(self.population.getGenerations())
                    self.temp_y.append(self.population.hitTarget)
                    self.d.on_running(self.temp_x,self.temp_y)
                    
                    self.population.fitness()
                    self.population.selection()
                    self.population.reproduction()
            elif event.type==pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    mouse_draging=True
                    x,y=pygame.mouse.get_pos()
                    obs.x=x
                    obs.y=y
            elif event.type==pygame.MOUSEMOTION:
                if mouse_draging:
                    x2,y2=pygame.mouse.get_pos()
                    obs.w=x2-obs.x
                    obs.h=y2-obs.y
                    self.temp_obs=obs
            elif event.type==pygame.MOUSEBUTTONUP:
                self.obs.append(Obstacle(obs.x,obs.y,obs.w,obs.h))
                obs=pygame.rect.Rect(0,0,0,0)
                mouse_draging=False
                self.temp_obs=[]
                
    
            
    def draw(self):
        self.screen.fill(self.bg_color)
        # draw target
        pygame.draw.rect(self.screen, (255,0,0),(self.width/2-20/2,24-20/2,20,20),0)
        
        # draw temperory obstacles
        if self.temp_obs:
            pygame.draw.rect(self.screen,(0,255,0),self.temp_obs,3)
        
        # draw all obstacles
        for obs in self.obs:
            pygame.draw.rect(self.screen,(0,255,0),obs.points(),0)
            
        # draw rockets
        for i in range(self.population_size):
            draw_centered(self.image,self.screen,self.population.population[i].location[0])
           
        self.info = self.small_font.render("life counter: "+str(self.life_counter),
                                       True,(0,0,0))
        self.info_generation = self.small_font.render("generation: "+str(self.population.getGenerations()),
                                       True,(0,0,0))
        self.info_hitTarget = self.small_font.render("hitTarget: "+str(self.population.hitTarget),
                                       True,(0,0,0))
        
        draw_centered(self.info,self.screen,(80,self.info.get_height()+10))
        draw_centered(self.info_generation,self.screen,(80,self.info.get_height()+self.info_generation.get_height()+10))
        draw_centered(self.info_hitTarget,self.screen,(80,self.info.get_height()+self.info_generation.get_height()+self.info_hitTarget.get_height()+10))
        
        pygame.display.flip()

if __name__ == "__main__":
    try:
         
        Game().run()
    except Exception as e:
        logging.error(traceback.format_exc())
    finally:
        pygame.quit()
