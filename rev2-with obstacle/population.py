import numpy as np
import math
from rocket import Rocket
from dna import DNA


class Population:
    def __init__(self,m,population_size,start_location,target,obs,lifetime):
        self.mutationRate = m
        self.population_size=population_size
        self.start_location=start_location
        self.target=target
        self.obs=obs
        self.population = []
        self.generations = 0
        self.probabilities=[]
        self.hitTarget=0
        for i in range(self.population_size):
            self.population.append(Rocket(self.start_location,self.target,self.obs,DNA(lifetime)))
        self.matingPool = []   
    def live(self):
        self.hitTarget=0
        for i in range(self.population_size):
            self.population[i].run()
            if self.population[i].hitTarget:
                self.hitTarget+=1
                
    def fitness(self):
        for i in self.population:
            i.fitness()
    def remap(self,value,old_min,old_max,new_min,new_max):
        if (old_max - old_min) ==0:
            return new_min
        return (((value-old_min)*(new_max-new_min))/(old_max-old_min))+new_min
    def softmax(self,x):
        x_temp=[]
        for i in x:
            x_temp.append(math.exp(i))
        x_total=sum(x_temp)
        x_output=[]
        for i in x_temp:
            x_output.append(i/x_total)
        return x_output
#    def selection(self):
#        self.probabilities=[]
#        maxFitness = self.getMaxFitness()
#        for i in self.population:
#            self.probabilities.append(self.remap(i.getFitness(),0,maxFitness,0,1))
#        self.probabilities=self.softmax(self.probabilities)

    def selection(self):
        self.matingPool=[]
        maxFitness=self.getMaxFitness()
        for i in self.population:
            fitnessNormal=self.remap(i.getFitness(),0,maxFitness,0,100)
            n = int(fitnessNormal*100)
            for j in range(n):
                self.matingPool.append(i)

            
            
#    def random_pick(self,some_list, probabilities):
#        x = np.random.uniform(0, 1)
#        cumulative_probability = 0.0
#        for item, item_probability in zip(some_list, probabilities):
#            cumulative_probability += item_probability
#            if x < cumulative_probability: break
#        return item
#    def reproduction(self):
#        population=[]
#        for _ in range(self.population_size):
#            dad = self.random_pick(self.population,self.probabilities).getDNA()
#            mum = self.random_pick(self.population,self.probabilities).getDNA()
#            child = dad.crossover(mum)
#            child.mutate(self.mutationRate)
#            population.append(Rocket(self.start_location,self.target,self.obs,child))
#        self.population=population
#        self.generations+=1
        
    def reproduction(self):
        for i in range(self.population_size):
            m=np.random.randint(0,len(self.matingPool))
            n=np.random.randint(0,len(self.matingPool))
            mom=self.matingPool[m]
            dad=self.matingPool[n]
            momgenes=mom.getDNA()
            dadgenes=dad.getDNA()
            child = momgenes.crossover(dadgenes)
            child.mutate(self.mutationRate)
            self.population[i]=Rocket(self.start_location,self.target,self.obs,child)
        self.generations+=1
        
    def getGenerations(self):
        return self.generations
    def getMaxFitness(self):
        record = 0
        for i in self.population:
            if i.getFitness()>record:
                record=i.getFitness()
        return record