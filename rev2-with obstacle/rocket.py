import numpy as np
import copy

class Rocket:
    def __init__(self,location,target,obs,dna):
        self.location = copy.copy(location)
        self.target=target
        self.velocity = np.zeros([1,2])        
        self.acceleration = np.zeros([1,2])
        self.my_fitness =0
        self.dna = dna
        self.geneCounter=0
        self.hitTarget=False
        self.hitObstacle=False
        self.obs=obs
        
        self.finishTime=0
        self.recordDist=10000
        
#    def fitness(self):
#        d=np.linalg.norm(self.location-self.target)
#        self.my_fitness = pow(1/d,2)
#        if self.hitTarget:
#            self.my_fitness*=2
#        if self.hitObstacle:
#            self.my_fitness*=0.1
    def fitness(self):
#        if self.recordDist <1:
#            self.recordDist=1
        self.my_fitness=(1/(self.finishTime*self.recordDist))
        self.my_fitness=pow(self.my_fitness,4)
        if self.hitObstacle:
            self.my_fitness*=0.01
        if self.hitTarget:
            self.my_fitness*=2
        
    def run(self):
        self.checkTarget()
        if (not self.hitTarget) and (not self.hitObstacle):
            self.applyForce(self.dna.genes[self.geneCounter])
            self.geneCounter = (self.geneCounter+1) % len(self.dna.genes)            
            self.velocity+=self.acceleration
            self.location+=self.velocity
            self.acceleration = np.zeros((1,2))
            
        
#    def checkTarget(self):
#        d = np.linalg.norm(self.location-self.target)
#        if d<12:
#            self.hitTarget = True
#        if self.obs.contains(self.location[0]):
#            self.hitObstacle=True
    def checkTarget(self):
        d = np.linalg.norm(self.location-self.target)
        if d < self.recordDist:
            self.recordDist = d
        if d<12:
            self.hitTarget=True
        if self.obs.contains(self.location[0]):
            self.hitObstacle=True
        if not self.hitTarget:
            self.finishTime+=1
            
    def applyForce(self,f):
        self.acceleration+=f
        
        
    
    def getFitness(self):
        return self.my_fitness
    
    def getDNA(self):
        return self.dna
    def getLocation(self):
        return self.location